<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Resource controllers:
// GET     /users                      index   users.index
// GET     /users/create               create  users.create
// POST    /users                      store   users.store
// GET     /users/{user}               show    users.show
// GET     /users/{user}/edit          edit    users.edit
// PUT     /users/{user}               update  users.update
// DELETE  /users/{user}               destroy users.destroy

Route::post('login', 'UserController@login');

// socialite
Route::get('login/{provider}', 'UserController@redirectToProvider');
Route::get('login/{provider}/callback', 'UserController@handleProviderCallback');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api')->group(function() {
    Route::resources([
        'photos' => 'PhotoController',
        'posts' => 'PostController',
        'projects' => 'ProjectController',
        'pages' => 'PageController',
        'comments' => 'CommentController',
        'users' => 'UserController',
    ]);
});
