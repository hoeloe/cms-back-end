<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'roles' => 3
    ];
});

$factory->define(App\Page::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'subtitle' => $faker->text,
        'photo_id' => 1,
        'lang' => 'EN',
        'content' => $faker->text,
    ];
});

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'content' => $faker->text,
    ];
});


$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'lang' => 'EN',
        'content' => $faker->text,
        'user_id' => 1,
    ];
});


$factory->define(App\Photo::class, function (Faker $faker) {
    return [
        'name' => 'default photo',
        'path' => 'photos/default.jpg',
        'user_id' => 1,

    ];
});



$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'client' => $faker->name,
        'lang' => 'EN',
        'content' => $faker->text,
        'photo_id' => 1,
    ];
});



$factory->define(App\OauthClient::class, function (Faker $faker) {
    return [
        'name' => '$faker->name',
        'secret' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'redirect' => 'http://localhost',
        'personal_access_client' => 0,
        'password_client' => 0,
        'revoked' => 0
    ];
});
