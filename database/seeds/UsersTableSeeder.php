<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Factory(App\User::class)->create([
            'name' => "admin",
            'email' => 'admin@admin.nl',
            'password' => bcrypt('Imm0admin'),
        ]);

    }
}
