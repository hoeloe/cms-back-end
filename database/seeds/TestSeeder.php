<?php

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $this->createUPCs();

        Factory(App\Photo::class)->create();

        Factory(App\Page::class)->create([
            'title' => 'first',
        ]);
        Factory(App\Page::class)->create([
            'title' => 'second',
        ]);

        Factory(App\Project::class, 4)->create([

        ]);

    }

    public function createUPCs($userCount = 3, $postCount = 3, $commentCount = 3) {
        $users = Factory(App\User::class, $userCount)->create([

        ]);
        foreach ($users as $user) {
            for ($i=0; $i < $postCount; $i++) {
                Factory(App\Comment::class, $commentCount)->create([
                    'post_id' => Factory(App\Post::class)->create([
                        'user_id' => $user->id,
                    ])->id,
                    'user_id' => $user->id,
                ]);

            }
        }

    }
}
