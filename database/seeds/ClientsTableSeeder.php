<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Factory(App\OauthClient::class)->create([
            'name' => 'angular password grant',
            'secret' => '2r1T9OGKLUnb53y6fkNd80HPQge2XJPtWNym3sgB',
            'redirect' => 'http://localhost',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => 0,
            'id' => 4
        ]);
    }
}
