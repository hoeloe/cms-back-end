<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="initial-scale=1.0, target-densitydpi=device-dpi" />
<meta name="viewport" content="initial-scale=1.0, width=device-height">
<script src="sign/libs/modernizr.js"></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<script src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript" src="sign/libs/flashcanvas.js"></script>
<style type="text/css">

	div {
		margin-top:1em;
		margin-bottom:1em;
	}
	input {
		padding: .5em;
		margin: .5em;
	}
	select {
		padding: .5em;
		margin: .5em;
	}
	#signatureparent {
		color:darkblue;
		background-color:darkgrey;
		padding:20px;
	}
	#signature {
		background-color:lightgrey;
	}
	html.touch #content {
		float:left;
		width:92%;
	}
	html.touch #scrollgrabber {
		float:right;
		width:4%;
		margin-right:2%;
	}
	html.borderradius #scrollgrabber {
		border-radius: 1em;
	}
    .sign {
        height: 10px;
        width: 30px;

    }
    td {
        border: 1px solid black;
        padding: 2px;
    }

</style>
</head>
<body>
    <div class="panel-heading"><h2>Handtekeningenactie voor behoud van fijne tafels en stoelen</h2></div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table>
                        <tr>
                            <td>Naam</td>
                            <td>Email</td>
                            <td>Geboorte datum</td>
                            <td>Handtekening</td>
                        </tr>
                        @foreach($signs as $sign)
                                <tr>
                                    <td>{{$sign->first_name}} {{$sign->last_name}} </td>
                                    <td>{{$sign->student_mail}} </td>
                                    <td>{{$sign->date_of_birth}}</td>
                                    <td class="sign">{!! $sign->signature !!} </td>
                                </tr>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
