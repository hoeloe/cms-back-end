@extends('layouts.app')
@section('content')
<div id="app">
    <passport-clients></passport-clients>
    <passport-personal-access-tokens></passport-personal-access-tokens>
    <passport-authorized-clients></passport-authorized-clients>

</div>
@endsection
