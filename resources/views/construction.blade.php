<style media="screen">
.container{
    display: grid;
    grid-gap: 5px;
    grid-template-columns: 1fr 2fr 2fr 2fr 1fr;
    grid-template-rows: 40px 100px 300px;
}
#logo {
    grid-column: 3 / 4;
    grid-row: 3;

}
.vlak{
    /* background-color: purple; */

}
.header {
    grid-column: 1 / 6;
    grid-row: 1 / 3;
    /* background-color: #802700; */
}

</style>
<div class="container">
    <div class="header"></div>
    <div class="vlak a"></div>
    <div class="vlak b"></div>
    <div class="vlak c"></div>
    <div class="vlak d"></div>

    <?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <!-- Created with Inkscape (http://www.inkscape.org/) -->

    <svg id="logo" class=""
       xmlns:dc="http://purl.org/dc/elements/1.1/"
       xmlns:cc="http://creativecommons.org/ns#"
       xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
       xmlns:svg="http://www.w3.org/2000/svg"
       xmlns="http://www.w3.org/2000/svg"
       xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
       xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
       version="1.1"
       id="svg3858"
       width="100%"
        height="100%"
        preserveAspectRatio="x200Y200 meet"
        viewBox="0 0 500 500"
       >
      <metadata
         id="metadata3864">
        <rdf:RDF>
          <cc:Work
             rdf:about="">
            <dc:format>image/svg+xml</dc:format>
            <dc:type
               rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
          </cc:Work>
        </rdf:RDF>
      </metadata>
      <defs
         id="defs3862" />
      <sodipodi:namedview
         pagecolor="#ffffff"
         bordercolor="#666666"
         borderopacity="1"
         objecttolerance="10"
         gridtolerance="10"
         guidetolerance="10"
         inkscape:pageopacity="0"
         inkscape:pageshadow="2"
         inkscape:window-width="640"
         inkscape:window-height="480"
         id="namedview3860" />
      <inkscape:clipboard
         style="font-variant-east_asian:normal;opacity:1;vector-effect:none;fill:#802700;fill-opacity:1;stroke:#000000;stroke-width:2.2670507;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         min="129.18923,332.91676"
         max="545.09652,835.67232" />
      <path
         inkscape:connector-curvature="0"
         id="path8938"
         d="M 144.36928,471.34337 107.77316,440.53308 99.073758,306.56938 C 94.289054,232.88942 89.856326,170.3184 89.223191,167.52253 87.333771,159.17854 73.555851,146.23358 40.168585,121.43365 9.2802292,98.490025 -4.2280746,86.76392 3.0738556,89.233274 4.971821,89.87534 41.537197,101.71544 84.33026,115.54526 c 42.79318,13.82982 78.20828,26.29383 78.70045,27.69777 0.72148,2.05867 10.27325,93.52876 10.27325,98.37952 0,0.74797 32.98311,1.35995 73.29593,1.35995 h 73.29593 l 3.01304,-42.71358 3.01297,-42.71365 37.86848,-67.319741 c 20.82765,-37.025953 40.81996,-72.346727 44.42706,-78.490746 l 6.55869,-11.1708698 -1.35239,21.2246928 c -0.7437,11.673634 -6.23622,109.113904 -12.20519,216.534054 l -10.85265,195.30947 -7.88814,6.45626 c -4.33871,3.55095 -23.17428,18.52751 -41.85713,33.28124 l -33.96858,26.82486 1.21946,-11.87505 c 0.67042,-6.5314 3.26457,-45.05284 5.7645,-85.60309 2.49967,-40.5504 4.94619,-78.00083 5.43681,-83.22323 l 0.89243,-9.49531 h -73.33033 -73.33032 l 0.11112,5.02696 c 0.0665,2.76484 1.99396,45.74479 4.30208,95.51116 2.30797,49.7664 3.98438,90.73704 3.7251,91.04598 -0.25965,0.30766 -16.93939,-13.30291 -37.06743,-30.24854 z"
         style="fill:#802700;fill-opacity:1;stroke:#000000;stroke-width:2.26705074;stroke-opacity:0" />
    </svg>

</div>
