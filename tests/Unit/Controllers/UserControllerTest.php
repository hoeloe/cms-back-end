<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Databasetransactions;
use App\Http\Controllers\UserController;

class UserControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
     public function testConstructor () {
         $controller = new UserController();
         $this->assertEquals(\App\User::class, $controller->model);
     }

    public function testIndex() {
        $request = new Request();
        $controller = new UserController();
        $this->assertTrue(
            $controller->index($request) instanceof
            \Illuminate\Http\JsonResponse
        );
    } //// TODO: test functionaliteit

}
