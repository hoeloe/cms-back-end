<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use DatabaseTransacions;

class UserModelTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetRightsAttribute()
    {
        $user  = Factory(\App\User::class)->make([
            'roles' => 254
        ]);

        $val = $user->rights;
        $testVal = [
          'comments',
          'pages',
          'photos',
          'posts',
          'projects',
          'users',
          'admin'
        ];
        $this->assertEquals($testVal, $val);
    }

    public function testHasRight() {
        $user1  = Factory(\App\User::class)->make([
            'roles' => 255
        ]);
        $user2  = Factory(\App\User::class)->make([
            'roles' => 0
        ]);
        $rights = [
          'active',
          'comments',
          'pages',
          'photos',
          'posts',
          'projects',
          'users',
          'admin'
        ];
        foreach ($rights as $right) {
            $this->assertTrue($user1->hasRight($right));
        }
        foreach ($rights as $right) {
            $this->assertFalse($user2->hasRight($right));
        }
    }
}
