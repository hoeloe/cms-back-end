<?php
//include "model/Entity.php";

class BlogItem extends Entity
{

    public $table_name = "blog_items";
    public $item_id;
    public $title;
    public $content;
    public $timestamp;
    public $lang;
    public $item_status;



    public function saveData() {
        if (empty($this->item_id)) {
            if ($this->validateItem()) {
                $query = "INSERT INTO `blog_items` (title, content, lang, item_status) values (?, ?, ?, ?)";
                $params = array($this->title, $this->content, $this->lang, $this->item_status);
                $this->runInsert($query, $params);
                $this->item_id = $this->pdo->lastInsertId();
            } else {
                echo "title already used";
            }
        } else {
            $query = "UPDATE `blog_items` SET title=?, content=?, lang=?, item_status=? WHERE item_id=?";
            $params = array($this->title, $this->content, $this->lang, $this->item_status, $this->item_id);
            $this->runInsert($query, $params);
        }
    }

    public function validateItem() {

        $stmt = $this->pdo->prepare("SELECT * FROM `blog_items` WHERE title=?");
        $stmt->execute(array($this->title));
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($stmt->rowCount($row) > 0) {
            echo "title already exists";
            return false;
        } else {
            return true;
        }
    }

    public function loadItem($id) {
        $data = $this->loadData($id, 'item_id');
        $this->item_id = $data['item_id'];
        $this->title = $data['title'];
        $this->content = $data['content'];
        $this->timestamp = $data['timestamp'];
        $this->lang = $data['lang'];
        $this->item_status = $data['item_status'];
    }
}
