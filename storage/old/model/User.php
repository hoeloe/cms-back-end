<?php
include "model/Entity.php";

class User extends Entity
{

    public $table_name = "users";
    public $username;
    public $first_name;
    public $last_name;
    public $email;
    public $password_hash;
    public $status;
    public $user_id;


    public function validateRegister() {
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            echo "Please enter valid email adress.";
            return false;
        } elseif (strlen($_POST['password1']) < 8) {
            echo "password too short.";
            return false;
        } elseif ($_POST['password1'] != $_POST['password2']) {
            echo "passwords don't match.";
            return false;
        } else {
            $query = $this->pdo->prepare("SELECT * FROM $this->table_name WHERE username=? ");
            $query->execute(array($_POST['new_username']));
            if ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                echo "username already taken.";
                return false;
            } else {
                return true;
            }
        }
    }


    public function saveData($id = null) {
        if ($id === null) {
            if ($this->validateRegister()) {
                $query = "INSERT INTO `users` (
                            username,
                            first_name,
                            last_name,
                            email,
                            password
                        ) values (?, ?, ?, ?, ?)";
                $params = [
                    $this->username,
                    $this->first_name,
                    $this->last_name,
                    $this->email,
                    $this->password_hash
                ];
                $this->runInsert($query, $params);
                $this->user_id = $this->pdo->lastInsertId();
            } else {
                echo "errors ocurred";
            }
        } else {
            $stmt = $this->pdo->prepare("UPDATE `users` WHERE id = ? ");
        }
    }


    public function loadUser($id) {
        $data = $this->loadData($id, 'user_id');
        $this->user_id = $id;
        $this->username = $data['username'];
        $this->first_name = $data['username'];
        $this->last_name = $data['last_name'];
        $this->email = $data['email'];
        $this->password_hash = $data['password'];
        $this->status = $data['status'];
    }

    public function validateLogin() {
        $stmt = $this->pdo->prepare("SELECT * FROM `users` WHERE username=?");
        $stmt->execute(array($this->username));
        $userRow = $stmt->fetch(PDO::FETCH_ASSOC);
        $password1 = $_POST['password'];
        $password2 = $userRow['password'];

        if ($stmt->rowCount() > 0) {
            if (password_verify($password1, $password2)) {
                $this->user_id = $userRow['user_id'];
                $this->loadUser($this->user_id);
                $message = "log in succesfull, welcome $this->username";
            } else {
                $message = "wrong username/password";
            }
        } else {
            $message = "wrong username/password";
        }
        echo "<div class='messageBox' ><p>$message</p></div>";
    }
}
