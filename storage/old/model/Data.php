<?php

class Data
{




    public $pages = [
        "Projects",
        "Progress",
        "About_me",
        "Contact",
        "Scrum",
        "Blog",
    ];

    public $projects = [
        "Pim Pam Pet",
        "Photoshop Hybrids",
        "Timetable",
        "Webpage Lorum FC",
        "Old Version Website"
    ];

    public $projectImages = [
        "Pim Pam Pet" => "img/projects/pimpampet.png",
        "Photoshop Hybrids" => "img/projects/octomonk.jpg",
        "Timetable" => "img/projects/timetable.png",
        "Webpage Lorum FC" => "img/projects/lorumfc.png",
        "Old Version Website" => "img/projects/oldsite.png"
    ];

    public $project_urls = [
        "Pim Pam Pet" => "sites/pimpampet/",
        "Photoshop Hybrids" => "?page=photoshop",
        "Timetable" => "sites/rooster/",
        "Webpage Lorum FC" => "sites/lorumsite/",
        "Old Version Website" => "sites/oldsite/"
    ];
}
