<?php
include 'model/Data.php';
class English extends Data
{

    public $page_names = [
    "Projects",
    "Progress",
    "About me",
    "Contact",
    "Scrum",
    "Blog"
    ];
    public $project_names = [
    "Pim Pam Pet",
    "Photoshop Hybrids",
    "Timetable",
    "Webpage Lorum FC",
    "Old Version Website"
    ];
    public $texts = [
    "about_me" => "First of all, welcome to my website. I am Jeroen Hielkema, a 25 years young man from Groningen and I recently started studying application development in Zwolle, in the Netherlands. As you can see I have begun this educational adventure by building a website and so far I am really enjoying it.",
    "photoshopHybrids" => "Since we are making websites, it can come in handy to have some Photoshop skills. We started with a class and our first assignment was to take two animals, and mix them up like a freshly shaken cocktail. To me, this sounded like a lot of fun and after laughing over and admiring some work of other students, I created these two ...fascinating species.",
    "timetable" => "One of our assignments was re-creating the timetable for our class, which seemed easy at first, but it cost quite a bit more effort to make such a table in HTML than it would in Excel or Word. Check it out below, touch it with your cursor and see what happens.",
    "lorumFC" => "As a test for out HTML skills, we got to make a specified website for a (obviously) fake football team. it feels good to say I that passed with ease. The result is displayed below.",
    "pimPamPet" => "As our latest project, to get familiar with Javascript, we recreated an old Dutch question game called Pim Pam Pet. I made my version look like good-old Windows vista",
    "firstWebsite" => "This was version 1.0 of my portfolio website. I like to keep the old one to see the progress. Since then I have implemented Javascript and PHP, and revisited my idea of design.",
    "progress"=> "These badges are awarded for completing tasks on Code Academy. I have completed the Javascript course and have recently started the Python course",
    "scrum"=>"Scrum is a method for working in projects. The term originates from Rugby. It is mainly used for softwaredevelopment and is aimed at multidisciplinary teams of about 7 people. The project is divided in sprints of about 2-4 weeks and which are reviewed each time to see what can be done better in the next one. \nAt the start of the project, one of the teammembers is assigned as scrum master, who guides the team according to the scrum method. Each sprint is divided into tiny parts which can be done within 30 minutes and these are written down on post-its, which in turn are stuck to the scrum board. This way it is accurately tracked what is done and by whom. This scurm board could ofcourse be digital. \nEvery working day starts with a stand-up where every member tells what he/she has done for the project and whether there are any problems, or work can continue. \nThis creates a flexible way of working and gives the client a clear view of the process."
    ];
    public $barmessage = "Hello, this my portfolio website v2.1, feel welcome to browse";

    public $formtext = [
    "title" => "Contact form",
    "name" => "Name",
    "email" => "Email adress",
    "subject" => "Subject",
    "content" => "Message"
    ];
}
