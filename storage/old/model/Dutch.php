<?php

class Dutch extends Data
{

    public $page_names = ["Projecten", "Voortgang", "Over_mij", "Contact", "Scrum", "Blog"];
    public $project_names = [
    "Pim Pam Pet",
    "Photoshop Hybriden",
    "Lesrooster",
    "Website Lorum FC",
    "Oude Versie Website"
    ];
    public $texts = [
    "about_me" => "Ik ben Jeroen Hielkema, een 24 jaar jonge man uit groningen, en ik ben dit jaar begonnen met de studie applicatie ontwikkelaar in zwolle. Zoals u kunt zien ben ik dit educatieve avontuur gestart met het leren van HTML en CSS, en inmiddels zijn javascript en PHP ook geimplementeerd.",
    "photoshopHybrids" => "Bij het bouwen van websites kan het handig zijn om wat skills te hebben in photoshop. We zijn bezig met een college photoshop en onze eerste opdracht was het volgende=> je neemt 2 dieren, van mooie grote foto's, en voegt ze bij elkaar tot een nieuw dier, half om half, net als gehakt. Dit leek me erg leuk en het lachen om en bewonderen van andermans werk heb ik deze twee ...bijzondere soorten geschapen.",
    "Lesrooster" => "Een van onze ondrachten was het re-creëren van on lesrooster in HTML. dit was lastiger dan verwacht maar na wat bloed zweet en tranen is het gelukt. ga er maar eens met uw muis overheen en kijk wat er gebeurt.",
    "Website Lorum FC" => "Als test voor onze HTML vaardigheden, moesten we een specifieke website nameken, de site was voor de (nep) voetbal club Lorum FC, en het ging vrij goed. het resultaat ziet u hier onder.",
    "progress"=> "Deze badges worden gegeven voor het volgens van lessen op Code academy. De javascript lessen heb ik afgerond, en ik ben zojuist begonnen met Python",
    "scrum"=> "Scrum is een methode voor het werken aan projecten. De term is afkomstig van rugby. Het wordt vooral gebruikt voor softwareontwikkeling en is gericht op multidisciplinaire teams van ongeveer 7 personen. \nHet project wordt opgedeeld in sprints van 2-4 weken, en na elke sprint wordt er teruggeblikt op hoe het verlopen is en wat er beter gedaan kan worden. aan het begin wordt iemand van het team als scrum master aangewezen, die het team begeleidt volgens de scrum methode. Elke sprint wordt opgedeelt in vele kleine onderdelen die binnen een half uur gedaan kunnen worden, en deze worden opgeschreven op post-its, die vervolgens op het Scrum bord worden geplakt. Zo wordt nouwkeurig bijgehouden wie wat doet en hoe het proces verloopt. Zo'n Scrum bord kan natuurlijk ook digitaal zijn. \nElke werkdag begint met een stand-up waarbij iedereen verteld wat hij/zij voor het team gedaan heeft, of er problemen zijn, en of er verder kan worden gewerkt. \nDit levert een flexibele manier van werken waarbij de opdrachtgever helder inzicht heeft in het proces."

    ];
    public $barmessage = "Hallo, dit is mijn portfolio website v2.1, u bent welkom om rond te kijken";


    public $formtext = [
    "title" => "Contact formulier",
    "name" => "naam",
    "email" => "Email adres",
    "subject" => "Onderwerp",
    "content" => "Bericht"
    ];
}
