<?php

/**
 * Created by PhpStorm.
 * User: hoeloe
 * Date: 7/6/17
 * Time: 12:10 PM
 */
class Comment extends Entity
{
    public $table_name = "comments";
    public $comment_id;
    public $target_id;
    public $username;
    public $content;
    public $timestamp;


    public function saveComment($id = null) {

        if ($this->validateComment()) {
            $query = "INSERT INTO `comments` (target_id, username, content) values (?, ?, ?)";
            $params = array($this->target_id, $this->username, $this->content);
            $this->runInsert($query, $params);
            $this->item_id = $this->pdo->lastInsertId();
        } else {
            echo "errors occurred";
        }
    }

    public function listComments($item_id) {

        $query = "SELECT `target_id`, `comment_id` FROM $this->table_name where target_id=?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(array($item_id));
        $list = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $list;
    }

    public function loadComment($id) {
        $data = $this->loadData($id, 'comment_id');
        $this->comment_id = $data['comment_id'];
        $this->target_id = $data['target_id'];
        $this->username = $data['username'];
        $this->content = $data['content'];
        $this->timestamp = $data['timestamp'];
    }

    public function validateComment() {
        return true;
    }
}
