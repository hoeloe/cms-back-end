<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Wandows Visto</title>
  <link rel="stylesheet" href="css/master.css" media="screen" title="no title">
  <script type="text/javascript" src="jquery-3.1.1.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
</head>
<body>
<div class="windows">
  <!-- containing game rules -->
  <div id="gameRules">
    <h1 id="title">PimPamPet-v0.7.exe</h1>
    <a onclick="langToggle()">&#62;&#62; Nederlands &#60;&#60;</a>
    <p>
    </p>
    <button onclick="showPlayers()" type="button" name="button">Got it</button>
  </div>
  <!-- containing the player input -->
  <div id="playerInput">

      <h1 id="title">PimPamPet-v0.7.exe ~ Players</h1>
      <p>
      <span>Player 1: </span><input class="pname" type="text" id="player1" placeholder="Enter player name"><br>
      <span>Player 2: </span><input class="pname" type="text" id="player2" placeholder="Enter player name"><br>
      <span>Player 3: </span><input class="pname" type="text" id="player3" placeholder="Enter player name"><br>
      <span>Player 4: </span><input class="pname" type="text" id="player4" placeholder="Enter player name"><br>
      <span>Player 5: </span><input class="pname" type="text" id="player5" placeholder="Enter player name"><br>
      <span>Player 6: </span><input class="pname" type="text" id="player6" placeholder="Enter player name"><br>
      <span>Player 7: </span><input class="pname" type="text" id="player7" placeholder="Enter player name">
      </p>
      <div id="submitPlayers">
        <button type="button" name="Submit" onclick="submitPlayers()">Ready</button>
      </div>

  </div>
  <!-- containing the game -->
  <div id="game">
    <h1 id="title">PimPamPet-v0.7.exe ~ Game</h1>
    <div id="letterdisk">
      <div id="letter">?</div>
    </div>
    <button type="button" name="spin" onclick="gamePlay()">Play!</button>

    <div id="teamlist">
      <span>Gamemaster:</span>
      <span id="p1"></span>
      <ul id="team1">
        <span>Green Team:</span>
        <li id="p2"></li>
        <li id="p4"></li>
        <li id="p6"></li>
      </ul>
      <span id="gPoints">Points: 0</span>
      <ul id="team2">
        <span>Yellow Team:</span>
        <li id="p3"></li>
        <li id="p5"></li>
        <li id="p7"></li>
      </ul>
      <span id="yPoints">Points: 0</span>
    </div>

    <div id="qCard">
      <span id="qEN"></span>
      <span id="qNL"></span>
    </div>
    <p id="answer">
      <input id="answerfield" type="text" onblur="this.value=''" name="answer" placeholder="Answer here ...">
      <button id="gbutton" type="button" onclick="addGPoints()">+</button>
      <button id="ybutton" type="button"onclick="addYPoints()">+</button>
    </p>

  </div>
  <div id="scoreboard">
    <h1 id="title">PimPamPet-v0.7.exe ~ Scoreboard</h1>
    <canvas id="canvas" width="500" height="400"></canvas>
      <script type="text/javascript" src="js/scoreboard.js"></script>
    <button type="button" name="button" onclick="scoreboard()">Draw!</button>
  </div>
  <div class="footer">
    <div id="start"><a href="../index.html">Home</a></div>
    <ul>
      <li onclick="toggleRules()">Rules</li>
      <li onclick="togglePlayers()">Players</li>
      <li onclick="toggleGame()">Game</li>
      <li onclick="toggleScoreboard()">Scoreboard</li>
    </ul>
    <div id="time"></div>

  </div>
</div>
</body>
</html>
