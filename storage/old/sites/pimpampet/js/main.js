"use strict"
var players = [];
var pointsdealt = true;
var regels = "Spelregels: \n\n- Je speelt het spel met 3 tot 7 spelers.\n\n- Een van de spelers word de spelleider, de anderen worden over twee teams verdeeld.\n\n- Druk op \"play!\" en er verschijnt een vraag en een letter.\n\n- Het team dat als eerste een passend antwoord heeft vult het in. \n\n- De spelleider controleert het. \n\n- is het correct, dan kent de spelleider een punt toe aan het team door op de knop met de kleur van het team te klikken.\n\n- is het fout, dan mag het andere team antwoorden, en krijgen zij een punt indie het correct is.\n\n- indien geen van beide teams een correct antwoord heeft gegeven, word verdergegaan naar de volgende vraag.\n\n- Op een gegeven moment zijn alle vragen beatwoord en verschijnt er een scorebord. de winnaar krijgt een koekje. ";
var rules = "Game rules: \n\n- The game is played with 3 to 7 players. \n\n- One of the players becomes the gamemaster, the rest are divided in two teams.\n\n- Press \"Play!\" and both a letter and a question appear.\n\n - The team that is first to answer the question fills it in.\n\n- The gamemaster checks wether it is correct.\n\n- If it is correct, then a point is assigned to the team by clickinghte button of corresponding color.\n\n- If it is incorrect, the other team gets a chance to answer, and gets a point if it is correct.\n\n- If both teams fail to answer correctly, no points are assigned, and the \"Play!\" button is again, pressed.\n\n- At some point the game will end.";
var questions = [
  "body part",
  "boy's name",
  "city in Europe",
  "cold drink",
  "colour",
  "country",
  "family member",
  "football club",
  "game",
  "gift",
  "girl's name",
  "hobby",
  "movie",
  "music instrument",
  "singer",
  "pet",
  "piece of furniture",
  "snack",
  "sport",
  "book",
  "tool",
  "vegetable",
  "warm drink",
  "famous actor/actress",
  "red thing",
  "blue thing"
];
var vragen = [
  "lichaamsdeel",
  "jongens naam",
  "stad in Europa",
  "frisdrank",
  "kleur",
  "land",
  "familie lid",
  "voetbal club",
  "spel",
  "onderdeel van een fiets",
  "meisjes naam",
  "hobbie",
  "film",
  "muziek instrument",
  "zanger(es)",
  "huisdier",
  "meubelstuk",
  "snack",
  "sport",
  "boek",
  "gereedschap",
  "groente",
  "warme drank",
  "beroemde acteur/actrice",
  "rood ding",
  "blauw ding"
];

var answersG;
var answersY;
var qlist = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
var letters = "ABCDEFGHIJKLMNOPRSTVWY";
var ypoints = 0;
var gpoints = 0;
var qc = 0;
var en = true;
var scoreboardScreen = false;
var gameScreen = false;
var playersScreen = false;
var rulesScreen = true;
var busy = false;


function saveAnswer(team) {
    if ($('#answerfield').text != null) {
        if (team = "yellow") {
            answersY += $('#answerfield').text;
        }
        if (team = "green") {
            answersG += $('#answerfield').text;
        }
    }

    
    $('#answerfield').val = "";
    console.log($('#answerfield').val);
    console.log(answersG, answersY);
}

function showTime() {
    var now = new Date();
    var hh = now.getHours();
    var mm = now.getMinutes();
    if (mm < 10) {
        mm = "0"+mm;
    }
    if (hh < 10) {
        hh = "0"+hh;
    }
    $("#time").text(hh+":"+mm);
}

window.onload = function () {
    showTime();
    $("#gameRules p").text(rules);
}


function toggleScoreboard() {
    $("#scoreboard").slideToggle();
}
function toggleRules() {
    $("#gameRules").slideToggle();
}
function togglePlayers() {
    $("#playerInput").slideToggle();
}
function toggleGame() {
    $("#game").slideToggle();
}

function langToggle() {
    if (en) {
        $("#gameRules p").text(regels);
        $("#gameRules a").text(">> English <<");
        en = false;
    } else if (!en) {
        $("#gameRules p").text(rules);
        $("#gameRules a").text(">> Nederlands <<");
        en = true;
    }

}

setInterval(showTime, 1000);

function addGPoints() {
    if (!pointsdealt) {
        gpoints++;
        pointsdealt = true;
        $("#gPoints").text("Points: "+gpoints);
        $("#gbutton").css("background-color", "grey")
        $("#ybutton").css("background-color", "grey")
        console.log("points green:"+gpoints);
        saveAnswer("green");
    }
}

function addYPoints() {
    if (!pointsdealt) {
        ypoints++;
        pointsdealt = true;
        $("#yPoints").text("Points: "+ypoints);
        $("#ybutton").css("background-color", "grey")
        $("#gbutton").css("background-color", "grey")
        console.log("points yellow:"+ypoints);
        saveAnswer("yellow");
    }
}


function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function randomletter() {
    for (var i = 0; i < letters.length; i++) {
        var letter = letters[Math.floor(Math.random()*21)];
        $("#letter").text(letter);
    }
}

function showPlayers() {
    $("#gameRules").fadeOut(200);
    setTimeout(function () {
        $("#playerInput").fadeIn(200);
    }, 201);
    rulesScreen = false;
    playersScreen = true;

}

function submitPlayers() {
    for (var i = 1; i <= 7; i++) {
        var x = $("#player"+i).val();
        if (x != "Enter player name" && x != "") {
            players.push(x);
        }
    }
    shuffle(qlist);
    shuffle(players);
    for (var i = 0; i < players.length; i++) {
        $("#p"+(i+1)).text(players[i]);
    }
    $("#playerInput").fadeOut(400);
    setTimeout(function () {
        $("#game").fadeIn(400);
    }, 401);
    playersScreen = false;
    gameScreen = true;
}

function newQuestion() {
    $()
    $("#qEN").text("Name a "+questions[qlist[qc]]+" starting with the letter");
    $("#qNL").text("Noem een "+vragen[qlist[qc]]+" beginnend met de letter");
    pointsdealt  = false;
    qc++;

}

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function gamePlay() {
    if (pointsdealt) {
        if (!busy ) {
            busy = true;
        
            if (qc < questions.length) {
                $("#letterdisk").addClass("lspin");
                $("#letter").addClass("fspin");
                $("#qCard").addClass("cspin");
                setInterval(function () {
                    if (busy) {
                        randomletter();}
                },50);
                setTimeout(function () {
                    $("#qCard").removeClass("cspin");
                }, 2000);
                setTimeout( function () {
                    newQuestion();
                },1000)
                setTimeout(function () {
                    $("#letterdisk").removeClass("lspin");
                    $("#letter").removeClass("fspin");
                    $("#ybutton").css("background-color", "yellow")
                    $("#gbutton").css("background-color", "green")
                    busy = false;
                }, 3000);
            } else {
                endGame();
            }
        }
    } else {
        alert("please add points to the winning team")
    }
}

function endGame() {
    $("#game").fadeOut(400);
    setTimeout(function () {
        $("#scoreboard").fadeIn(400);
    }, 401);
    setTimeout(function () {
        scoreboard();
    }, 1000);
}
