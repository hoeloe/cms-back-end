"use strict"


var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var winner;
console.log(canvas);
console.log(ctx);
function scoreboard() {
    ctx.beginPath();
    ctx.fillStyle="black";
    ctx.font = "14px Arial";
    ctx.fillText("And the winner is...",10,50);
    ctx.closePath();
    setTimeout(function () {
        drawPillars();
    }, 500);
    setTimeout(function () {
        drawWinner();
    }, 1800);


}

function drawPillars() {
    console.log(gpoints);
    for (var i = 0; i <= ypoints; i++) {
        ctx.beginPath();
        ctx.fillStyle="yellow";
        ctx.fillRect(250,(canvas.height-(10*i)),60,12)
        ctx.closePath();
    }
    setTimeout(function () {
        for (var i = 0; i <= gpoints; i++) {
            ctx.beginPath();
            ctx.fillStyle="green";
            ctx.fillRect(340,(canvas.height-(10*i)),60,12);
            ctx.closePath();
        }
    }, 1000);
}


function drawWinner() {
    if (gpoints>ypoints) {
        ctx.beginPath();
        ctx.fillStyle="green";
        ctx.font = "28px Arial";
        ctx.fillText("Team Green!",20,150);
        ctx.font = "17px Arial";
        ctx.fillText("With "+gpoints+" points.",10,180);
        ctx.closePath();
    } else {
        ctx.beginPath();
        ctx.fillStyle="yellow";
        ctx.font = "28px Arial";
        ctx.fillText("Team Yellow!",20,150);
        ctx.font = "17px Arial";
        ctx.fillText("With "+ypoints+" points.",10,180);
        ctx.closePath();
    }
}
