-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 28, 2017 at 11:46 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_items`
--

CREATE TABLE `blog_items` (
  `item_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lang` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `item_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_items`
--

INSERT INTO `blog_items` (`item_id`, `title`, `content`, `timestamp`, `lang`, `item_status`) VALUES
(5, 'The reason why we can\'t just post things online', '<p>Lorem ipsum dolor sit amet, sed debet intellegam ad. In cum molestie partiendo mediocritatem, no oblique legendos percipitur ius, pro ne doctus sensibus adolescens. Deleniti appetere an nam, eu vim dolor quidam. Per diam appellantur et. Nam apeirian dissentias et, quo oratio possim salutandi ei, ad nisl aliquam saperet eam.</p>\r\n\r\n<p>Pri ne tale consul. Nonumes suscipit pertinacia ea mel, consul mollis percipitur has ei, ei vis dicat consequuntur. Rebum erroribus corrumpit per in. Per ea diam denique. Congue populo mucius in has. Ad mei reprimique consectetuer conclusionemque, sea dicam nostrum suscipiantur cu, te omnes essent sea.</p>\r\n\r\n<p>Elit nullam ne vel, at saperet vocibus sapientem sit. Quo dictas mentitum mnesarchum in. Choro mucius ex nec, an possim oportere nam. Congue audiam vituperata eu usu, vix eu choro volumus complectitur, mollis oblique complectitur mel at.</p>\r\n', '2017-07-27 08:19:54', 'English', 2);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `target_id` int(11) DEFAULT NULL,
  `username` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `login_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `ip_adress` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `first_name`, `last_name`, `email`, `status`, `password`) VALUES
(9, 'jeroen', 'jeroen', 'hielkema', 'jphielkema@gmail.com', 3, '$2y$10$Xx9aw8tKD/alu6JUpd15y.bVDHPvPWr6rrZOSFIt8BtvpdPZ.iqOi'),
(11, 'hoeloe', 'hoeloe', 'hoeloe', 'jeroensateprikker@hotmail.com', 1, '$2y$10$dziD9Y4PrdKzOr84n653qO23jWVGkLV7y4UeTDwFUqaUjyk54floW');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_items`
--
ALTER TABLE `blog_items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_items`
--
ALTER TABLE `blog_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
