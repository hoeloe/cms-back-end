"use strict"
var faceClasses = ["one", "two", "three", "four", "five", "six"];
var cubeFacings = ["rotateX(-90deg) rotateY(0deg)","rotateX(0deg) rotateY(0deg)", "rotateX(0deg) rotateY(-90deg)", "rotateX(0deg) rotateY(-180deg)", "rotateX(0deg) rotateY(-270deg)", "rotateX(90deg) rotateY(-180deg)"];
var menuItems = document.getElementsByClassName('menuItem');
var xAngle = 0;
var yAngle = 0;
var allowKeys = true;
var cube1Unfolded = false;

var cube1 = document.getElementById("main_cube");
var faces = [document.getElementById('f1'), document.getElementById('f2'), document.getElementById('f3'), document.getElementById('f4'), document.getElementById('f5'), document.getElementById('f6')];


function showFace(face) {
    if (!cube1Unfolded) {
        cube1.classList.remove("cubespin");
        cube1.style.webkitTransform = face;
    }
}





function unfoldCube() {
    for (var i = 0; i < faces.length; i++) {
        faces[i].classList.toggle(faceClasses[i]);
        faces[i].classList.toggle("faceGrid");
    }
    cube1.classList.toggle("grid");
    cube1.classList.toggle("cube");
    showFace(cubeFacings[1]);
    cube1Unfolded = !cube1Unfolded;
}

document.getElementById("menu").addEventListener("mouseleave", function () {
    setTimeout(function () {
        cube1.classList.add("cubespin");
    }, 2000);
});
var items = [];
for (var i = 0; i < menuItems.length; i++) {
    items[i]=menuItems[i];

    items[i].addEventListener("mouseenter", function () {
        showFace(cubeFacings[items.indexOf(this)]);
    });

    menuItems[i].addEventListener("click", function () {



    });
}
