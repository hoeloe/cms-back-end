"use strict"
var menu = true;
var menuAnimated = false;

$(document).ready(function () {

    $('#menu_switch').on('click', function () {
        toggleMenu();
    });
});


function toggleMenu() {
    if (menu&&!menuAnimated) {
        menuAnimated = true;
        $('#menu_switch').removeClass('flip_hor');
        $('#menu').addClass('menu_sway');
        $('#menu').removeClass('menu_show');
        setTimeout(function () {
            $('nav').slideToggle(200);
            $('#cube1').removeClass('grid');
            $('#cube1').addClass('widegrid');
        }, 300);
        setTimeout(function () {
            menuAnimated=false;
            menu = false;
        }, 500);
    } else if (!menu&&!menuAnimated) {
        menuAnimated = true;
        $('#cube1').removeClass('widegrid');
        $('#cube1').addClass('grid');
        $('#menu_switch').addClass('flip_hor');
        $('nav').slideToggle(200);
        setTimeout(function () {
            $('#menu').removeClass('menu_sway');
            $('#menu').addClass('menu_show');

        }, 300);
        setTimeout(function () {
            menuAnimated=false;
            menu=true;
        }, 500);
    }
}
