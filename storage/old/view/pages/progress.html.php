<div class="holder">
  <div id="cube1" class="grid">
    <div class="flextile">
      <h3>Code Academy</h3>
      <p><?php echo $locale->texts['progress'] ?></p>
    </div>
    <div class="flexTile">
      <img class="badge" src="img/badges/200badges.png" alt="">
      <img class="badge" src="img/badges/phpbadge.png" alt="">
      <img class="badge" src="img/badges/pythonbadge.png" alt="">
    </div>
  </div>
</div>
