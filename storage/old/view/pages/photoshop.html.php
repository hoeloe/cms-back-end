<div id="holder">
  <div id="cube1" class="grid">
    <div class="flextile">
      <h3><?php echo $locale->project_names[1]; ?></h3>
      <p><?php echo $locale->texts['photoshopHybrids']; ?></p>
    </div>
    <div class="flextile">
      <img class="smaller_image" src="img/projects/chicken.jpg" alt="">
      <h2 class="symbol">+</h2>
      <img class="smaller_image" src="img/projects/kangaroo.jpg" alt="">
      <h2 class="symbol">=</h2>
      <img src="img/projects/chickaroo.jpg" alt="">
    </div>
    <div class="flextile">
      <img class="smaller_image" src="img/projects/monkey.jpg" alt="">
      <h2 class="symbol">+</h2>
      <img class="smaller_image" src="img/projects/octopus.jpg" alt="">
      <h2 class="symbol">=</h2>
      <img src="img/projects/octomonk.jpg" alt="">
    </div>
  </div>
</div>
