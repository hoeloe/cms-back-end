<div id="holder">
    <div id="cube1" class="grid">
        <?php
        foreach ($this->locale->projects as $project) { ?>
            <a id=<?php echo $project; ?>
               href= <?php echo $this->locale->project_urls[$project]; ?>
               class="project">
                <h3><?php echo $this->locale->project_names[array_search($project, $this->locale->projects)] ?></h3>
                <img src=<?php echo $this->locale->projectImages[$project] ?> alt=""/>
            </a>
        <?php } ?>

    </div>
</div>

<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
