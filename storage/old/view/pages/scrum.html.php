<div class="holder">
  <div id="cube1" class="grid">
    <div class="flextile">
      <h3>The Scrum method</h3>
      <p>
        <?php echo $locale->texts['scrum']; ?>
      </p>
    </div>
    <div class="flextile">
      <img src="img/scrumboard.png" alt="">
      <p>*A Scrum board in progress*</p>
    </div>
  </div>
</div>
