<div class="holder">
    <div class="grid">
        <div class="blogtile">
<div class="blogmenu">
    <ul>
        <?php

        if ($this->userController->isLoggedIn()) {
            ?>
            <a href="?page=blog&blade=login"><li>Log out</li></a>
            <a href="?page=blog&blade=profile"><li>Profile</li></a>
            <a href="?page=blog&blade=items&action=show"><li>List items</li></a>

            <?php
            if ($this->isAdmin()) {
                ?>
                <a href="?page=blog&blade=control_panel"><li>Control panel</li></a>
                <a href="?page=blog&blade=new_item"><li>New item</li></a>
                <a href="?page=blog&blade=userlist"><li>User List</li></a>

                <?php
            }
        }
        ?>



    </ul>
</div>
            <?php

            include "view/blogblades/$this->blade.html.php";
            ?>


        </div>
    </div>
</div>
