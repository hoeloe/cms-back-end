* {
    margin: 0;
    padding: 0;
}

body {
    background-color: <?php echo $bg2_color; ?>;
    background-attachment: fixed;
    background-size: cover;
    font-size: 12px;
    font-family: monospace;
    color: <?php echo $fg_color; ?>;

}

a {
    color: inherit;
    text-decoration: none;
}

a:visited {
    color: inherit;
    text-decoration: none;
}

h2 {
    font-size: 50px;
}

::-webkit-scrollbar {
    background-color: <?php echo $bg2_color; ?>;
    margin-top: 40px;
}

::-webkit-scrollbar-button {
    border: 2px solid <?php echo $border_color; ?>;
}

::-webkit-scrollbar-track {
    background-color: <?php echo $bg_color; ?>;
    border: 2px solid <?php echo $fg_color; ?>;
}

::-webkit-scrollbar-thumb {
    background-color: <?php echo $fg_color; ?>;
}

::-webkit-scrollbar-corner {
    background-color: <?php echo $fg_color; ?>;
}

::-webkit-resizer {
    background-color: <?php echo $fg_color; ?>;
}

.topbar {
    position: fixed;
    height: 40px;
    width: 100%;
    background-color: <?php echo $bg_color; ?>;
    /*box-shadow: 0 4px 50px <?php echo $fg_color; ?>;*/
    border-bottom: 1px solid <?php echo $fg_color; ?>;
    z-index: 13;
    <!-- -webkit-animation: greenNeon 3s ease-in-out infinite; -->

}

#homebutton {
    position: absolute;
    left: 80px;
    font-size: 25px;
    font-family: monospace;
    background-color: <?php echo $bg_color; ?>;
    color: <?php echo $fg_color; ?>;
    border-right: 2px solid <?php echo $border_color; ?>;
    height: 35px;
    padding-left: 10px;
    padding-top: 5px;
    padding-right: 10px;
    text-align: center;
    z-index: 14;
}

#settings img {
    height: 40px;
    position: relative;
    float: right;
    background-color: <?php echo $fg_color; ?>;

}

#menu_switch {
    height: inherit;
    width: 40px;
    -webkit-transition: .3s;
    z-index: 15;
    cursor: pointer;
}

#menu_switch_holder {
    height: 40px;
    width: 80px;
    position: absolute;
    padding-left: 20px;
    padding-right: 15px;
    background-color: <?php echo $bg_color; ?>;
    border-right: 2px solid <?php echo $border_color; ?>;
    z-index: 10;

}

.flip_hor {
    -webkit-transform: rotateX(180deg);
    -webkit-transition: .3s;
}

.topbar p {
    position: absolute;
    text-align: center;
    width: 100%;
    top: 15px;
}

/*dropdown languages switch*/
.dropdown {
    position: relative;
    display: inline-block;
    font-family: monospace;
    font-size: 20px;
    float: right;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: <?php echo $bg_color; ?>;
    color: <?php echo $fg_color; ?>;
    min-width: 160px;
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    color: <?php echo $fg_color; ?>;
    padding: 7px 20px;
    text-decoration: none;
    display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {
    background-color: <?php echo $fg_color; ?>;
    color: <?php echo $bg_color; ?>;
}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
    display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
    background-color: <?php echo $fg_color; ?>;
}

.dropbtn {
    height: 40px;
    width: 160px;
    font-size: 25px;
    background-color: <?php echo $bg_color; ?>;
    color: <?php echo $fg_color; ?>;
    border: none;
    border-left: 2px solid <?php echo $border_color; ?>;

}

/*navigation menu*/

nav {
    position: fixed;
    display: inline-block;
    float: left;
    height: 400px;
    margin-left: 40px;
    margin-top: 0;
    -webkit-perspective: 1000px;
    -webkit-perspective-origin: 50% 60%;
    -moz-perspective: 1000px;
    -moz-perspective-origin: 50% 60%;
    border-left: 4px solid <?php echo $border_color; ?>;
    z-index: 10
}

nav ul {
    list-style-type: none;
    width: 150px;
    margin-right: 0;
    margin-top: 100px;
}

nav ul li {
    position: relative;
    background-color: <?php echo $bg_color; ?>;
    height: 30px;
    padding-top: 5px;
    padding-left: 5px;
    margin-bottom: 10px;
    width: 180px;
    font-family: Monospace;
    font-size: 25px;
    color: <?php echo $fg_color; ?>;
    transition: .2s;
    cursor: pointer;
    border: 2px solid <?php echo $border_color; ?>;
    border-radius: 4px;
}

.menu_show {
    transform: rotateY(50deg);
    -moz-transform: rotateY(50deg);
    -webkit-transform: rotateY(50deg);
    transition: .3s;
}

.menu_sway {
    transform: rotateY(90deg) translateZ(-100px);
    -moz-transform: rotateY(90deg) translateZ(-100px);
    -webkit-transform: rotateY(90deg) translateZ(-100px);
    transition: .3s;
}

#menu li:hover {
    width: 200px;
    height: 35px;;
    font-size: 28px;
    margin-bottom: 5px;
    -webkit-transform: translateZ(50px);
    -moz-transform: translateZ(50px);
    transform: translateZ(50px);
    transition: .2s;

}

.active_item {
    width: 200px;
    height: 35px;;
    font-size: 28px;
    margin-bottom: 5px;
    -webkit-transform: translateZ(50px);
    -moz-transform: translateZ(50px);
    transform: translateZ(50px);
    color: <?php echo $hl_color; ?>;
    border-color: <?php echo $hl_color; ?>;
}

#footer {
    position: fixed;
    width: 100%;
    height: 15px;
    font-size: 12px;
    background-color: #111;
    border-top: 2px solid #666;
    color: #444;
    text-align: center;
    bottom: 0;

}

#footer > span {
    border-right: 2px solid black;

}

#footer span {
    padding: 0px 20px 0px 20px;
}

/*big cube on homepage*/
#holder {
    -webkit-perspective: 1800px;
    -webkit-perspective-origin: 50% 100px;
    -moz-perspective: 800px;
    -moz-perspective-origin: 50% 100px;
    transition: .4s;
    margin-bottom: 40px;
}

.cube {
    left: calc(50% - 200px);
    top: 100px;
    position: relative;
    height: 400px;
    width: 400px;
    margin-top: 0;
    -webkit-transition: -webkit-transform .3s linear;
    -webkit-transform-style: preserve-3d;
    -moz-transition: -moz-transform .3s linear;
    -moz-transform-style: preserve-3d;

}

/*class for shrinking*/
.small {
    -webkit-transform: scale(0.5);
    transition: .3s;
}

.c1Face {
    position: absolute;
    height: 360px;
    width: 360px;
    padding: 20px;
    font-size: 10px;
    font-family: monospace;
    color: <?php echo $fg_color; ?>;
    background-color: <?php echo $bg_color; ?>;
    border: 2px solid <?php echo $border_color; ?>;

}

.c1Face:hover {
    background-color: rgba(100, 100, 200, 0.3);
}

.one {
    -webkit-transform: rotateX(90deg) translateZ(200px);
    -moz-transform: rotateX(90deg) translateZ(200px);
    transition: .5s;
}

.two {
    -webkit-transform: translateZ(200px);
    -moz-transform: translateZ(200px);
    transition: .5s;
}

.three {
    -webkit-transform: rotateY(90deg) translateZ(200px);
    -moz-transform: rotateY(90deg) translateZ(200px);
    transition: .5s;
}

.four {
    -webkit-transform: rotateY(180deg) translateZ(200px);
    -moz-transform: rotateY(180deg) translateZ(200px);
    transition: .5s;
}

.five {
    -webkit-transform: rotateY(-90deg) translateZ(200px);
    -moz-transform: rotateY(-90deg) translateZ(200px);
    transition: .5s;
}

.six {
    -webkit-transform: rotateX(-90deg) translateZ(200px) rotate(180deg);
    -moz-transform: rotateX(-90deg) translateZ(200px) rotate(180deg);
    transition: .5s;
}

.project {
    position: relative;
    display: inline-flex;
    transition: .5s;
    margin: 10px;
    text-align: left;
    padding: 10px;
    background-color: rgba(0, 0, 0, 0.5);
    /*box-shadow: 1px 1px 8px 1px <?php echo $fg_color; ?>;*/
    border-radius: 4px;
    height: 300px;
    width: 300px;
    transition: .3s;
}

.project:hover {
    -webkit-transform: translateZ(30px);
    transition: .3s;
}

.project img {
    position: absolute;
    max-height: 250px;
    max-width: 280px;
    box-shadow: 2px 2px 3px 3px inset <?php echo $fg_color; ?>;
    border-left: 2px solid <?php echo $border_color; ?>;
    border-right: 2px solid <?php echo $border_color; ?>;
    padding: 10px;
    margin-top: 40px;
}

.project h3 {
    position: absolute;
    width: 300px;
    font-size: 20px;
    color: <?php echo $fg_color; ?>;
    font-family: monospace;
    border-bottom: 1px solid <?php echo $border_color; ?>;
    padding-bottom: 10px;
    text-align: center;
}

.grid {
    position: relative;
    width: 80%;
    align-content: space-around;
    transition: .3s;
}

.widegrid {
    position: relative;
    width: 100%;
    top: 100px;
    left: 50px;
    align-content: space-around;
    transition: .3s;
}

.flextile {
    position: relative;
    display: inline-block;
    vertical-align: top;
    height: auto;
    width: auto;
    margin: 10px;
    margin-bottom: 25px;
    border-top: 2px solid <?php echo $border_color; ?>;
    border-bottom: 2px solid <?php echo $border_color; ?>;
    border-radius: 4px;
    font-size: 15px;
    background-color: rgba(0, 0, 0, 0.5);
    transition: .3s;
}

.flextile img {
    max-width: 500px;
    max-height: 500px;
    padding: 0 10px 0 10px;
    margin: 10px 0 10px;

}

.flextile .smaller_image {
    max-width: 250px;
    max-height: 250px;
    padding: 0 10px 0 10px;
    margin: 10px 0 10px;
    display: inline-block;
}

.flextile p {
    padding: 10px 20px 20px 20px;
    margin: 10px 0 10px;
    max-width: 800px;
}

.flextile h3 {
    padding: 20px;
    color: <?php echo $fg_color; ?>;
    border-bottom: 1px solid <?php echo $border_color; ?>;
}

.symbol {
    position: relative;
    bottom: 120px;
    display: inline-block;
}

#myface {
    height: 200px;
}

#myface:hover {
    background-color: #999999;

}


.form input {
    height: 30px;
    width: 150px;
    border: 2px solid <?php echo $fg_color; ?>;
    background-color: #111;
    color: white;
    padding-left: 10px;
}

.register_form p {
    padding: 0;

}

.form {
    padding: 50px 100px 50px 100px;

}

.new_item form > input{
    width: 600px;
}

.contact_form input {
    border: 2px solid <?php echo $fg_color; ?>;
    height: 20px;
    width: 180px;
    border-radius: 4px;
}

.contact_form input:active {
    border: 2px solid #00cc00;
    border-radius: 4px;
}

#contact_message {
    margin-top: 30px;
    margin-bottom: 30px;
    width: 560px;
    height: 400px;
}

.cubespin {
    -webkit-animation: cubespin 100s linear infinite;
    -webkit-transform: rotateX(-20deg);
}

/***************************Blog area**************************/






.blogtile {
    position: relative;
    display: inline-block;
    width: 90%;
    min-height: 800px;
    border: 2px solid <?php echo $border_color; ?>;
    border-radius: 6px;
}

.blogItem {
    padding: 10px;
    color: <?php echo $blog_text_color; ?>;
    width: inherit;
    box-shadow: inset 0px 0px 100px <?php echo $blog_shadow_color;  ?>;


}

.blogItem h2{
    color: <?php echo $blog_text_color; ?>;
    border-bottom: 2px dotted <?php echo $blog_text_color; ?>;
    margin-bottom: 10px;
    margin-top: 20px;
}

.blogItem p{
    padding: 10px;
}


.itemListTile{
    height: 200px;
    width: 300px;
    overflow: hidden;
    margin: 10px;
}
.itemListTile h2{
    font-size: 30px;
    width: 900px;

}
.itemListTile p{
    padding-top: 5px;
    margin-top: 0px;
}

.blogmenu ul{
    list-style-type: none;
    border-bottom: 1px solid <?php echo $border_color; ?>;
}
.blogmenu ul li{
    display: inline-block;
    border-left: 2px solid <?php echo $border_color; ?>;
    padding-left: 3px;
    font-size: 20px;
}

.blogmenu ul li:hover{
    background-color: <?php echo $hl_color; ?>;
}

.commentTile{
    display: block;
    border-left: 1px solid <?php echo $border_color; ?>;
    border-top: 1px solid <?php echo $border_color; ?>;
}

.commentTile h3{
    border-bottom: 2px solid <?php echo $border_color; ?>;
    color: <?php echo $fg_color; ?>;
    font-size: 30px;

}


.commentField{
    margin: 0;
    margin-bottom: 30px;
    margin-left: 20px;
    height: 100px;
    width: 400px;
    border-radius: 4px;
}

.comment input{
    position: relative;
    top: -115px;
    background-color: <?php echo $hl_color; ?>;
    border: none;
    width: 50px;
    color: white;
    
}

.comment{
    min-height: 50px;
    width: 500px;
    padding-top: 3px;
    border-top: 2px dotted <?php echo $border_color; ?>;

}
.comment p{
    padding-left: 15px;
    padding-bottom: 10px;
}


.profile form{
    text-align: left;
}



/***********************************Animations*****************************************/


@-webkit-keyframes rainbowMenu {
    0% {
        border-color: <?php echo $fg_color; ?>;
        color: <?php echo $fg_color; ?>;

3    20% {
    border-color: #CCCC00;
    color: #CCCC00;

}
40% {
    border-color: #CC0000;
    color: #CC0000;;

}
60% {
    border-color: #CC00CC;
    color: #CC00CC;

}
80% {
    border-color: <?php echo $fg_color; ?>;
    color: <?php echo $fg_color; ?>;

}
100% {
    border-color: <?php echo $fg_color; ?>;
    color: <?php echo $fg_color; ?>;

}
}

@-webkit-keyframes rainbowShadow {
    0% {
        box-shadow: 0 4px 50px <?php echo $fg_color; ?>;

    }
    20% {
        box-shadow: 0 4px 50px #CCCC00;

    }
    40% {
        box-shadow: 0 4px 50px #CC0000;

    }
    60% {
        box-shadow: 0 4px 50px #CC00CC;

    }
    80% {
        box-shadow: 0 4px 50px <?php echo $fg_color; ?>;
    }
    100% {
        box-shadow: 0 4px 50px <?php echo $fg_color; ?>;
    }
}

@-webkit-keyframes greenNeon {
    0% {
        box-shadow: 0 4px 60px <?php echo $fg_color; ?>;
    }

    50% {
        box-shadow: 0 4px 40px <?php echo $fg_color; ?>;
    }

    100% {
        box-shadow: 0 4px 60px <?php echo $fg_color; ?>;
    }
}

@-webkit-keyframes cubespin {

    0% {
        -webkit-transform: rotateY(0deg);
    }
    100% {
        -webkit-transform: rotateY(3600deg);

    }

}
