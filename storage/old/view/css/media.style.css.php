<?php
header("Content-type: text/css");

?>
@media only screen
and (min-device-width: 0px)
and (max-device-width: 700px){

    /*navigation menu*/
    nav{
        position: fixed;
        display: inline-block;
        float: left;
        height: 400px;
        margin-left: 0px;
        margin-top: 0;
        width: 50px;
        -webkit-perspective: 1000px;
        -webkit-perspective-origin: 50% 60%;
        -moz-perspective: 1000px;
        -moz-perspective-origin: 50% 60%;
        border-right: 4px solid #00cccc;
        z-index: 10
    }
    nav ul{
        width: 0;
        overflow: hidden;
        list-style-type: none;
        margin-right: 0;
        margin-top: 100px;
        transition: .3s;
    }

    nav ul li{
        position: fixed;
        background-color: #000000;
        height: 30px;
        padding-top: 5px;
        padding-left: 5px;
        margin-bottom: 10px;
        width: 180px;
        font-family: Monospace;
        font-size: 25px;
        color: #00cccc;
        transition: .2s;
        cursor: pointer;
        border: 2px solid #00cccc;
    }
    #menu{
        transform: rotateY(0deg);
        -moz-transform: rotateY(0deg);
        -webkit-transform: rotateY(0deg);
    }



    nav ul li:hover{
        width: 200px;
        height: 35px;
        font-size: 28px;
        margin-bottom: 5px;
        -webkit-transform: translateZ(0px);
        transition: .2s;
        -webkit-animation: rainbowMenu 10s infinite;

    }

}
