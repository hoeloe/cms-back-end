<?php

header("Content-type: text/css");

$fg_color = "#999999";
$border_color = "#551111";
$bg_color =  "#111115";
$bg2_color = "#222222";
$hl_color = "#ccaaaa";

$blog_text_color = "#00000";
$blog_shadow_color = "rgba(0, 0, 0, 0.3)";

include 'main.style.css.php';
include 'blog.style.css.php';
include 'media.style.css.php';
