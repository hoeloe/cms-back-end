
/***************************Blog area**************************/

.blogtile {
    position: relative;
    display: inline-block;
    width: 90%;
    min-height: 800px;
    border: 2px solid <?php echo $fg_color; ?>;


}

.itemListTile{
    height: 200px;
    width: 300px;
    overflow: hidden;

}
.itemListTile p{
    padding-top: 5px;
    margin-top: 0px;
}

.blogmenu ul{
    list-style-type: none;
    border-bottom: 1px solid <?php echo $fg_color; ?>;
}
.blogmenu ul li{
    display: inline-block;
    border-left: 2px solid <?php echo $fg_color; ?>;
    padding-left: 3px;
    font-size: 20px;
}

.blogmenu ul li:hover{
    background-color: black;
}

.commentTile{
    display: block;
    border-left: 1px solid <?php echo $fg_color; ?>;
    border-top: 1px solid <?php echo $fg_color; ?>;

}
commentField > input{
    height: 100px;
    width: 250px;

}

.profile form{
    text-align: left;
}




.blogtile {
    position: relative;
    display: inline-block;
    width: 90%;
    min-height: 800px;
    border: 2px solid <?php echo $fg_color; ?>;
}

.blogItem {
    padding: 10px;
    color: #DDDDFF;
    width: inherit;
    background-color: #2e383c;
    background: -moz-linear-gradient(left, rgba(127,127,127,0.65) 0%, rgba(0,0,0,0) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left, rgba(127,127,127,0.65) 0%,rgba(0,0,0,0) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right, rgba(127,127,127,0.65) 0%,rgba(0,0,0,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a67f7f7f', endColorstr='#00000000',GradientType=1 ); /* IE6-9 */
}

.blogItem h2{
    color: #EEEEEE;
    border-bottom: 2px dotted #666;
    margin-bottom: 10px;
    margin-top: 20px;
}

.blogItem p{
    padding: 10px;
}


.itemListTile{
    height: 200px;
    width: 300px;
    overflow: hidden;
    margin: 10px;

}
.itemListTile p{
    padding-top: 5px;
    margin-top: 0px;
}

.blogmenu ul{
    list-style-type: none;
    border-bottom: 1px solid <?php echo $fg_color; ?>;
}
.blogmenu ul li{
    display: inline-block;
    border-left: 2px solid <?php echo $fg_color; ?>;
    padding-left: 3px;
    font-size: 20px;
}

.blogmenu ul li:hover{
    background-color: black;
}

.commentTile{
    display: block;
    border-left: 1px solid <?php echo $fg_color; ?>;
    border-top: 1px solid <?php echo $fg_color; ?>;
}


.commentField{
    margin: 0;
    margin-bottom: 30px;
    margin-left: 20px;
    height: 100px;
    width: 400px;
}

.comment input{
    position: relative;
    top: -115px;
    background-color: #2e383c;
    border: none;
    width: 50px;
    color: white;
}

.comment{

}




.profile form{
    text-align: left;
}