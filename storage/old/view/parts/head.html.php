<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=0.5"/>
    <meta charset="utf-8">
    <meta name="theme-color" content="#00CCCC">
    <link rel="stylesheet" type="text/css" href="view/css/style.css.php" media="screen" title="no title">
    <link rel="icon" href="img/jphcyber.ico">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
    <title>JPH <?php
    if (isset($_GET["page"])) {
        $page = $_GET['page'];
        echo " - $page";
    }
        ?></title>
</head>
