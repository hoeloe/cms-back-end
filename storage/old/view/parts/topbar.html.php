<div class="topbar">
  <div id="menu_switch_holder"><img id="menu_switch" class="flip_hor" src="img/menuswitch.png" alt=""></div>
  <a id="homebutton" href="/">/Home</a>
    <?php
    if (isset($_SESSION['username'])) {
        $name = $_SESSION['username'];
        echo "<p>Hello $name</p>";
    } else {
        echo "<p>$locale->barmessage</p>";
    }

    ?>
    <div id="settings"><img src="img/settings.png"></div>
  <div class="dropdown">
    <button class="dropbtn">language</button>
    <div class="dropdown-content">
      <a href="?lang=en">English</a>
      <a href="?lang=nl">Nederlands</a>
    </div>
  </div>
</div>
