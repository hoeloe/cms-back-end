<nav>
  <ul id="menu" class='menu_show'>
    <?php
    $i=0;
    foreach ($this->locale->pages as $page) : ?>
      <a id=<?php echo "item$i";
        $href_page = str_replace(' ', '', strtolower($page))?>
        class="menuItem"
        href=<?php echo "?page=$href_page"?>>
        <li class=<?php if (isset($_GET['page'])) {
            if ($_GET['page'] == $href_page) {
                echo "active_item";
            }
        } else {
    echo "";
          } ?>><?php echo $this->locale->page_names[$i];?></li>
      </a>
    <?php $i++;
    endforeach; ?>
  </ul>
</nav>
