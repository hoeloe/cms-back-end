
    <div class="flextile">
        <?php
        if ($this->userController->isLoggedIn()) {
        ?>
            <div class="logout">
                <p>You are logged in as <?php echo $this->userController->user->username ?></p>
                <form class="form" action="index.php?page=blog&action=logout" method="post">
                    <p><input type="submit" value="Log out"></p>
                </form>
            </div>
        <?php } else {
                ?>
            <div id="login">
                <form class="form" action="index.php?page=blog&action=login" method="post">
                    <p><input type="text" name="username" placeholder="username" required></p>
                    <p><input type="password" name="password" placeholder="password" required></p>
                    <p><input type="submit" name="login" value="Log in"></p>
                </form>
            </div>
            <a href="?page=register">Click here to register!</a>
        <?php
    }
        ?>

    </div>
