<?php foreach ($this->blogItemController->itemList as $blogItem) : ?>
    <?php if ($blogItem->item_status == 1 || $this->isAdmin()) : ?>
        <div class="blogItem <?php if (count($this->blogItemController->itemList) > 1) :
            echo 'itemListTile';
                             endif ?>">
            <a href="?page=blog&blade=items&action=show&item=<?php echo $blogItem->item_id ?>">
                <h2>“<?php echo $blogItem->title; ?>”</h2></a>
            <?php if ($this->isAdmin()) :
?><a
                href="?page=blog&blade=new_item&action=show&item=<?php echo $blogItem->item_id ?>">
                    Edit</a>
            <?php endif; ?>
            <p><?php echo $blogItem->timestamp; ?></p>
            <p class="itemContent"><?php echo $blogItem->content; ?></p>
        </div>
        <?php if (count($this->blogItemController->itemList) === 1) : ?>
            <div class="commentTile">
                <h3>Comments:</h3>
                <?php if (!empty($this->blogItemController->commentController)) :
                    foreach ($this->blogItemController->commentController->comments as $comment) : ?>
                        <div class="comment">
                            <p><?php echo $comment->username; ?> @ <?php echo $comment->timestamp; ?></p>
                            <p><?php echo $comment->content; ?></p>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
                <div class="comment">
                    <form id="commentForm" method="post"
                          action="?page=blog&blade=items&action=show&item=<?php echo $blogItem->item_id ?>&comment=place">
                        <!--                    <input type="text" name="comment" placeholder="type your comment here..." class="commentField">-->
                        <textarea name="comment" class="commentField" placeholder="Type your comment here"></textarea>
                        <input type="submit" value="Place">
                        <input type="hidden" name="username"
                               value="<?php echo $this->userController->user->username ?>">
                    </form>
                </div>
            </div>

        <?php endif; ?>
    <?php endif; ?>
<?php endforeach; ?>