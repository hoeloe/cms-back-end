<?php

/**
 * Created by PhpStorm.
 * User: hoeloe
 * Date: 7/3/17
 * Time: 4:15 PM
 */

 
include "controller/BlogItemController.php";

class BlogController
{
    public $blade;
    public $admin;
    public $loggedIn;
    public $userController;
    public $blogItemController;



    function __construct() {
        $this->userController = new UserController();

        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $this->performAction($action);
        }
        if (isset($_GET['comment'])&&$_GET['comment']==='place') {
            $this->blogItemController->commentController->placeComment($_GET['item']);
        }

        if (isset($_GET['blade']) && $this->userController->isLoggedIn()) {
            $this->blade = $_GET['blade'];
            if ($this->blade == "new_item" || $this->blade == "control_panel" || $this->blade == "userlist") {
                if (!$this->isAdmin()) {
                    $this->blade = "login";
                }
            }
        } else {
            $this->blade = "login";
        }
        include "view/pages/blog.html.php";
    }

    public function isAdmin() {
        if ($this->userController->user->status === "3") {
            return true;
        } else {
            return false;
        }
    }




    public function performAction($action) {
        switch ($action) {
            case 'register':
                $this->userController = new UserController();
                $this->userController->registerNewUser();

                break;
            case 'login':
                $this->userController = new UserController();
                $this->userController->logIn();
                break;
            case 'logout':
                $this->userController = new UserController();
                $this->userController->logOut();
                break;
            case 'save':
                $this->blogItemController = new blogItemController();
                if (isset($_GET['item'])) {
                    $this->blogItemController->updateItem();
                } else {
                    $this->blogItemController->createItem();
                }
                break;
            case 'show':
                if (isset($_GET['item'])) {
                    $id = $_GET['item'];
                    $this->blogItemController = new blogItemController();
                    $this->blogItemController->showItem($id);
                } else {
                    $this->blogItemController = new blogItemController();
                    $this->blogItemController->listAllItems();
                }
            default:
        }
    }
}
