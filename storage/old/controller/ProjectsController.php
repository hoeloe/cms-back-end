<?php

/**
 * Created by PhpStorm.
 * User: hoeloe
 * Date: 6/23/17
 * Time: 2:46 PM
 */
class ProjectsController
{
    public $locale;

    function __construct($locale) {
        $this->locale = $locale;
    }

    public function list() {
//        $project_names = $this->locale->project_names;
        foreach ($this->locale->projects as $project) {
            include 'view/pages/projects.html.php';
        }
    }
}
