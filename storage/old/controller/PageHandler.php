<?php

include "model/English.php";
include "model/Dutch.php";
include "controller/UserController.php";
include "controller/ProjectsController.php";
include "controller/BlogController.php";

/**
 * controller for page request handing
 */
class PageHandler
{

    public $data;
    public $locale;
    public $controller;




    function __construct($request) {
// handle request
        $this->data = $request;

// set language
        if (isset($_GET['lang'])) {
            $lang = $_GET['lang'];
            // register the session
            $_SESSION['lang'] = $lang;
        } elseif (isset($_SESSION['lang'])) {
            $lang = $_SESSION['lang'];
        } else {
            $lang = "en";
        }
        switch ($lang) {
            case 'en':
                $this->locale = new English();
                break;
            case 'nl':
                $this->locale = new Dutch();
                break;
            default:
                $this->locale = new English();
                break;
        }
    }


    public function useHead() {
        $locale = $this->locale;
        include 'view/parts/head.html.php';
    }

    public function showPart($part) {
        $locale = $this->locale;
        include "view/parts/$part.html.php";
    }

    /**
     *
     */
    public function showPage() {
        $locale = $this->locale;


        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = "home";
        }


        switch ($page) {
            case 'profile':
                $userController = new UserController();
                $userController->showProfile();
                break;
            case 'blog':
                $blogController = new BlogController();
                break;
            default:
                include "view/pages/$page.html.php";
        }
    }

    public function showPageBeta() {
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = "home";
        }

        if (isset($_GET['action'])) {
            $action = $_GET['action'];
        }

        $this->controller = new $page.'Controller';
        $this->controller->$action();
    }
}
