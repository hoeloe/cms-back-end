<?php

/**
 * Created by PhpStorm.
 * User: hoeloe
 * Date: 7/6/17
 * Time: 3:40 PM
 */
include "model/Comment.php";

class CommentController
{
    public $comments = [];



    public function loadComments($item_id) {
        $loader = new Comment();
        $list = $loader->listComments($item_id);
        foreach ($list as $comment) {
            $this->comments[$comment['comment_id']] =  new Comment();
            $this->comments[$comment['comment_id']]->loadComment($comment['comment_id']);
        }
    }

    public function placeComment($item_id) {
        $comment = new Comment();
        $comment->target_id = $item_id;
        $comment->username = $_POST['username'];
        $comment->content = $_POST['comment'];
        $comment->saveComment();
        header("Refresh:0; url=index.php?page=blog&blade=items&action=show&item=$item_id");
    }
}
