<?php

include 'model/User.php';

class UserController
{

    public $user;


    function __construct() {
        if (isset($_SESSION['user_id'])) {
            $this->user = new User();
            $this->user->loadUser($_SESSION['user_id']);
        }
    }

    public function registerNewUser() {


        $this->user = new User();
        $this->user->username = $_POST['new_username'];
        $this->user->email = $_POST['email'];
        $this->user->first_name = $_POST['first_name'];
        $this->user->last_name = $_POST['last_name'];
        $this->user->password_hash = password_hash($_POST['password1'], PASSWORD_DEFAULT);
        $this->user->saveData();
    }

    public function logIn() {
        $this->user = new User();
        $this->user->username = $_POST['username'];
        $this->user->validateLogin();
        $_SESSION['user_id'] = $this->user->user_id;
        $_SESSION['username'] = $this->user->username;
    }

    public function logOut() {
        unset($_SESSION['user_id']);
        unset($_SESSION['username']);
    }
    function getUsernameById($user_id) {
        $nameGetter = new User();
        $nameGetter->loadData($user_id, 'user_id');
        return $nameGetter->username;
    }


    public function showProfile() {
        $this->user = new User();
        $this->user->loadUser($_SESSION['user_id']);
        $title = $this->user->username . "'s profile";
        include "view/pages/profile.html.php";
    }

    public function isLoggedIn() {
        if (isset($_SESSION['user_id'])) {
            return true;
        } else {
            return false;
        }
    }
}
