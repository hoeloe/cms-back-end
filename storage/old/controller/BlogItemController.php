<?php

/**
 * Created by PhpStorm.
 * User: hoeloe
 * Date: 7/4/17
 * Time: 8:36 PM
 */
include "model/BlogItem.php";
include "controller/CommentController.php";

class blogItemController
{

    public $itemList = [];
    public $commentController;


    public function createItem() {
        $this->itemList[0] = new blogItem();
        $this->itemList[0]->title = $_POST['title'];
        $this->itemList[0]->content = $_POST['content'];
        $this->itemList[0]->lang = $_POST['lang'];
        $this->itemList[0]->item_status = $_POST['item_status'];
        $this->itemList[0]->saveData();
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRlMTQ1ZDE0MzA5MGZkZmMzYTVlNmI3ZTkxZDdlODlhMGZkZWI4ZTE2MTI4ZTNmMDhiZGQ2MDFjODFkZWI3Y2NhMTU4MGNmZWU1MmZmNTEwIn0.eyJhdWQiOiIxIiwianRpIjoiZGUxNDVkMTQzMDkwZmRmYzNhNWU2YjdlOTFkN2U4OWEwZmRlYjhlMTYxMjhlM2YwOGJkZDYwMWM4MWRlYjdjY2ExNTgwY2ZlZTUyZmY1MTAiLCJpYXQiOjE1MDEzNTA1NzMsIm5iZiI6MTUwMTM1MDU3MywiZXhwIjoxNTMyODg2NTczLCJzdWIiOiIyNjgiLCJzY29wZXMiOltdfQ.lEtZ_6_upg6adT5yXVnoclN-1kwbS7QA-sAOKi45C5lRT6FAU6p3ZkXvKybMVVGRQw9cmPZk0UyHPoy3ZDX4oLBXgSy_Z1TlDOkG70OLBTKpvjhXp10-WQrQhluxTrbBFj0Ec6oYW6Lh_u8XXPyL2LprLv-MctsU2WEh03jOVtsROhrYGYGvsBaCBWO10PfnHG2cv4MuM5XTiP6H7SHaXXjMpEfGXncO6hOHqPXcWM_Gp4R0t9FJlETu-9fuJ2Iv_jndLqdEdKjM1i_uPYFsjNdAZn3Z07lors8xqJbTRNNw9wtgBJX1x4d8VyzZY2_SOOKU0NRANPCVrH33iehMI_KEEy2WUb8o3zdRXnpJL86PUFpdqOwqanKUtau30s2raZFC1k-Cr061CCvrvMwmu5LcffYuiYbbSfLVrgsd9bQdEITiaQd0u9HrPQLXIwxHG5BFJtulB-Jv_K3PQIdcjYOMf3PY1GKwf3jqGzM_w0CbcoZwD0Njsx4LNbJmCjW-m-t8sYcNG_VWG9ZD4PAwpvr3G19Ylq2fs3v9d4ZoGL9u6w-vywJDkfP9yjYR-v9x0j0EOOj5C-5MaqGBmR-hBhvXE2lIFkugRqL0iBDAEL-8KWi28Y0wbHHZ9vpkx7fZczDy0ffdY30Isb26G19-ypbt5rqR8BXNw-GgEgKCVKI';
        $post = json_encode(['weblog' => $_POST['content'], 'title' => $_POST['title']]);
        $url = 'https://www.newdeveloper.nl/api/weblog';
        $ch = curl_init();
        $header = array();
        $header[] = 'Content-length: '.strlen($post);
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer '.$token;
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => $post,

            CURLOPT_RETURNTRANSFER => true,
        ]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($ch);
    }

    public function updateItem() {
        $this->itemList[0] = new blogItem();
        $this->itemList[0]->item_id = $_GET['item'];
        $this->itemList[0]->title = $_POST['title'];
        $this->itemList[0]->content = $_POST['content'];
        $this->itemList[0]->lang = $_POST['lang'];
        $this->itemList[0]->item_status = $_POST['item_status'];
        $this->itemList[0]->saveData();
    }

    public function listAllItems() {
        $item = new BlogItem();
        $list = $item->listData();
        foreach ($list as $item) {
            $this->itemList[$item['title']] = new BlogItem();
            $this->itemList[$item['title']]->loadItem($item['item_id']);
        }
    }

    public function showItem($id) {
        $this->itemList[0] = new blogItem();
        $this->itemList[0]->loadItem($id);
        $this->commentController = new CommentController();
        $this->commentController->loadComments($id);
    }
}
