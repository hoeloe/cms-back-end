<?php

    session_start();

// --------------debugging------------
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include "controller/debug/debugfunctions.php";

// -------------site code------------
  include 'controller/PageHandler.php';
  $pageHandler = new PageHandler($_SERVER['REQUEST_URI']);
  $pageHandler->useHead();
    ?>
  <body>
<?php

$pageHandler->showPart("topbar");
$pageHandler->showPart("menu");
$pageHandler->showPage();
$pageHandler->showPart("footer");

    ?>
</body>


<?php
//--------more debug------

