<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AppendInfo;


class Comment extends Model
{
    use AppendInfo;

    protected $fillable = [
        'content'
    ];

    public function getFillable() {
        return $this->fillable;
    }

    public function post() {
        return $this->belongsTo('App\Post');

    }


}
