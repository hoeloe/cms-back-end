<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $rights = [
      0b00000001 => 'active',
      0b00000010 => 'comments',
      0b00000100 => 'pages',
      0b00001000 => 'photos',
      0b00010000 => 'posts',
      0b00100000 => 'projects',
      0b01000000 => 'users',
      0b10000000 => 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'rights'
    ];

    public function getFillableAttribute() {
        return $this->fillable;
    }


    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function posts() {
        return $this->hasMany('App\Post');
    }

    public function photos() {
        return $this->hasMany('App\Photo');
    }




    public function getRightsAttribute(){
      $rights = [];
        for ($i=1; $i <= 2**(count($this->rights)); $i*=2) {
            if ($this->roles & $i ) {
                $rights[] = $this->rights[$i];
            }
        }
        return $rights;
    }

    public function hasRight($right) {
        if ($this->roles & array_search($right, $this->rights)) {
            return true;
        } else {
            return false;
        }
    }
}
