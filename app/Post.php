<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\AppendInfo;

class Post extends Model
{
    use AppendInfo;


    protected $fillable = [
        'title',
        'lang',
        'content',
        'user_id'
    ];


    protected $appends = [
        'commentCount'
    ];

    protected $with = [
        'comments'
    ];


    public function getFillable() {
        return $this->fillable;
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getCommentCountAttribute(){
        return $this->comments->count();
    }


}
