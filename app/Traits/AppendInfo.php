<?php

namespace App\Traits;


use Carbon\Carbon;
use App\User;

trait AppendInfo
{

    protected $appendages = [
        'author',
        'age',
        'lastEdit'
    ];

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->appends = array_merge($this->appends, $this->appendages);
    }


    public function getAuthorAttribute() {
        return User::find($this->user_id)->name;
    }

    public function getLastEditAttribute() {
        return $this->getTimeElapsed($this->updated_at);

    }

    public function getAgeAttribute() {
        return $this->getTimeElapsed($this->created_at);

    }

    private function getTimeElapsed($dateString) {
        $now = Carbon::now();
        $created = Carbon::parse($dateString);
        if ($now->diffInYears($created) > 1) {
            return $now->diffInYears($created) . ' years';
        } elseif ($now->diffInMonths($created) > 1) {
            return $now->diffInMonths($created) . ' months';
        } elseif ($now->diffInWeeks($created) > 1) {
            return $now->diffInWeeks($created) . ' weeks';
        } elseif ($now->diffInDays($created) > 1) {
            return $now->diffInDays($created) . ' days';
        } elseif ($now->diffInHours($created) > 1) {
            return $now->diffInHours($created) . ' hours';
        } elseif ($now->diffInMinutes($created) > 1) {
            return $now->diffInMinutes($created) . ' minutes';
        } else {
            return '< 1 minute';
        }
    }

}
