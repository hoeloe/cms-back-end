<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use App\Post;
use App\Policies\PostPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        \App\Post::class => \App\Policies\PostPolicy::class,
        \App\Photo::class => \App\Policies\PhotoPolicy::class,
        \App\User::class => \App\Policies\UserPolicy::class,
        \App\Comment::class => \App\Policies\CommentPolicy::class,
        \App\Comment::class => \App\Policies\CommentPolicy::class,
    ];

    /**.
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Laravel\Passport\PassportServiceProvider::class;
        Passport::routes();
    }
}
