<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AppendInfo;


class Page extends Model
{
    // use AppendInfo;

    protected $fillable = [
        'title',
        'subtitle',
        'lang',
        'content',
        'icon',
    ];

    public function getFillable() {
        return $this->fillable;
    }

    public function photo() {
        $this->hasOne('App\Photo');
    }
}
