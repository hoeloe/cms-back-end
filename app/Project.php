<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AppendInfo;


class Project extends Model
{
    use AppendInfo;

    protected $fillable = [
        'title',
        'product owner',
        'lang',
        'content',
        'url',
        'icon',
    ];

    public function getFillable() {
        return $this->fillable;
    }

    public function photo() {
        $this->hasOne('App\Photo');
    }

}
