<?php

namespace App\Http\Middleware;

use Closure;

class CrossOrigin
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Log::info('corsmiddleware');
        if ($request->isMethod('OPTIONS')) {
            return response('', 200)
                ->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE, PATCH')
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Headers', 'origin, authorization, accept, content-type, x-xsrf-token, x-csrf-token,  X-Auth-Token,  X-Requested-With');
        } else {
            $response = $next($request);
            $response->header('Access-Control-Allow-Origin', '*');
            return $response;
        }
    }
}
