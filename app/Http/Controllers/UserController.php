<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use GuzzleHttp\Client as GuzzleClient;
use Laravel\Socialite\Facades\Socialite;

class UserController extends BaseController
{

    public $successStatus = 200;

    /**pi
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $data  = [
                    'id' => '4',
                    'secret' => '2r1T9OGKLUnb53y6fkNd80HPQge2XJPtWNym3sgB',
                    'user' => Auth::user(),
            ];
        return response()->json($data)->header('Access-Control-Allow-Origin', '*');
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $providerUser = Socialite::driver($provider)->stateless()->user();

        if (User::where('email', $providerUser->email)->first()) {
            $res = ['Error' => 'Email adress already has an account'];
            return response()->json($res);
        } else {
            dd($providerUser);
            $user = new User();
            $user->email = $providerUser->Email;
            $user->password = $provider;
            
        }
    }
}
