<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as LaravelController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use App\Post;

class BaseController extends LaravelController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $model = '';


    protected $allowed = [
        'orderBy',
        'where',
        'all',
        'find',
        'posts',
        'orderBy',
        'with'
    ];

    function __construct () {
        $this->model = substr("App\\", 0, -1) .
            substr(get_class($this), 20, -10);

    }


    public function index(Request $request) {
        \Log::info($this->model);

//        $this->authorize('view', $this->model);



        $queries = [];
        if ( $request->query() ) {
            foreach ($request->query() as $key => $value) {
                if ( in_array($key, $this->allowed) ) {
                    if ( strpos($value, ',') ) {
                        $value = explode(',', $value);
//                        \Log::info(['value', $value]);
                    }
                    $queries[$key] = $value;
                }
            }
            if (count($queries)) {
                $queryCount = 0;
                $finalQuery = $this->model;
                foreach ($queries as $function => $params) {
                    if ( is_array($params)) {
                        $finalQuery = call_user_func_array([$finalQuery, $function], $params);
                    } elseif ($params) {
                        $finalQuery = call_user_func([$finalQuery, $function], $params);
                    } else {
                        $finalQuery = call_user_func($finalQuery .'::'. $function);
                    }
                }
                $data = $finalQuery->get();
            }
        } elseif (in_array('all', $this->allowed)) {
            $data = call_user_func($this->model .'::all');
        } else {
            $data = ['error' => 'Not Allowed'];
        }
        return response()->json($data)->header('Access-Control-Allow-Origin', 'http://localhost:4200');
    }


    public function store(Request $request) {
        $this->authorize('create', $this->model);
        $data = array_merge($request->input(), ['user_id' => Auth::user()->id]);
        call_user_func([$this->model, 'create'], $data);


    }



    public function show($id)
    {
        $this->authorize('view', $this->model);

        $object = call_user_func($this->model . '::find', $id);
        return response()->json($object);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $object = call_user_func($this->model . '::find', $request->input('id'));
        $this->authorize('update', $object);
        $object->update($request->input());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', $this->model);
        $object = call_user_func($this->model . '::destroy', $id);
    }
}
