<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PhotoController extends BaseController
{
    public function store (Request $request) {
        $photo = new Photo();
        $photo->name = $request->input('name');
        $photo->user_id = Auth::user()->id;
        $file = $request->file('photo');
        $photo->path = Storage::putFile(
            'photos',
            $file
        );
            $photo->save();
    }
}
