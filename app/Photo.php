<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AppendInfo;


class Photo extends Model
{
    use AppendInfo;

    protected $fillable = [
        'title',
        'path',
    ];

    public function getFillable() {
        return $this->fillable;
    }

    public function project() {
        $this->belongsTo('App\Project');
    }
    public function page() {
        $this->belongsTo('App\Page');
    }
    public function user() {
        $this->belongsTo('App\User');
    }
}
